﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinSCP;

namespace testFtp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // Setup session options
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Sftp,
                    HostName = "192.168.0.224",
                    UserName = "ssuv",
                    Password = "uvzssuv" ,
                    SshHostKeyFingerprint = "ssh-rsa 2048 01:db:4d:b3:ad:aa:bc:8b:16:b8:20:84:6b:2e:93:24",
                };

                using (Session session = new Session())
                {
                    // Connect
                    session.DisableVersionCheck = true;
                    session.Open(sessionOptions);

                    const string remotePath = "/tmp/";                       
                    const string localPath = "c:\\temp\\";

                    // Get list of files in the directory
                    RemoteDirectoryInfo directoryInfo = session.ListDirectory(remotePath);

                    // Select the most recent file
                    RemoteFileInfo latest =
                        directoryInfo.Files
                            .Where(file => !file.IsDirectory)
                            .OrderByDescending(file => file.LastWriteTime)
                            .FirstOrDefault();

                    // Any file at all?
                    if (latest == null)
                    {
                        throw new Exception("No file found");
                    }

                    // Download the selected file
                    session.GetFiles(session.EscapeFileMask(remotePath + latest.Name), localPath).Check();
                    session.RemoveFiles(session.EscapeFileMask(remotePath + latest.Name));
                }

             }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e);
             }
            Console.ReadLine();
        }
    }
}
